const path = require('path');
const webpack = require('webpack');
const HtmlWebPackPlugin = require("html-webpack-plugin");
const Dotenv = require('dotenv-webpack');
const dotenv = require('dotenv');

module.exports = (env, options) => {
  // 引入全域env環境變數
  dotenv.config({ path: path.resolve(__dirname, `./.env`) });
  // console.log('webpack env', env);
  // console.log('webpack options', options);
  // console.log('process.env.FRONTEND', process.env.FRONTEND);
  return {
    entry: './src/index.tsx',
    // output: {
    //   filename: 'bundle.js',
    // },
    output: {
      publicPath: '/'
    },
    module: {
      rules: [
        {
          test: /\.css$/i,
          use: ["style-loader", "css-loader"]
        },
        {
          test: /\.less$/i,
          use: [
            { loader: "style-loader" },
            { loader: "css-loader" },
            {
              loader: "less-loader",
              options: {
                lessOptions: {
                  javascriptEnabled: true,
                }
              }
            }
          ]
        },
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: ['babel-loader', 'ts-loader']
        },
        {
          test: /\.tsx?$/,
          exclude: /node_modules/,
          use: 'ts-loader'
        },
        {
          test: /\.(png|jpe?g|gif)$/i,
          use: [
            {
              loader: 'file-loader',
              options: { name: 'statics/images/[name].[ext]' }
            },
          ],
        }
      ],
    },
    resolve: {
      alias: {
        components: path.resolve(__dirname, 'src/components/'),
        containers: path.resolve(__dirname, 'src/containers/'),
        containers_portal: path.resolve(__dirname, 'src/containers_portal/'),
        components_portal: path.resolve(__dirname, 'src/components_portal/'),
        static: path.resolve(__dirname, 'src/statics/'),
        utils: path.resolve(__dirname, 'src/utils/'),
        services: path.resolve(__dirname, 'src/services/'),
        "@": path.resolve(__dirname, "src/"),
      },
      extensions: ['.js', '.jsx', '.tsx', '.ts'],
      modules: [
        path.resolve(__dirname, 'src/'),
        path.resolve(__dirname, 'node_modules/'),
      ]
    },
    plugins: [
      new HtmlWebPackPlugin({
        template: path.resolve(__dirname, 'src/index.html'),
        filename: "./index.html"
      }),
      // new Dotenv({
      //   path: './.env', // Path to .env file (this is the default)
      //   safe: true, // load .env.example (defaults to "false" which does not use dotenv-safe)
      // })
      // new webpack.DefinePlugin({
      //   FRONTEND: JSON.stringify(process.env.FRONTEND)
      // })
    ],
  };
};
