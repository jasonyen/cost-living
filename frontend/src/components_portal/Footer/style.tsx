import styled from "styled-components";

export const Container = styled.div`
    background: #333333;
    min-height: 200px;
    color: #ffffff;
    font-size: 14px;
    display: flex;
    justify-content: center;
    align-items: center;
`;
