import React from 'react';
import * as Style from './style';

const Footer = () => {
    return (
        <Style.Container>
            Powerd by Jason Jhang
        </Style.Container>
    );
}

export default Footer;