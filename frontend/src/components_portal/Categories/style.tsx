import styled from "styled-components";

export const Cards = styled.div`
    max-width: 1200px;
    margin: 0 auto;
    display: grid;
    gap: 20px;
    @media only screen and (max-width: 556px) {
        grid-template-columns: repeat(1, 1fr);
    }
    @media only screen and (min-width: 557px) and (max-width: 800px) {
        grid-template-columns: repeat(2, 1fr);
    }
    @media only screen and (min-width: 801px) {
        grid-template-columns: repeat(3, 1fr);
    }
`;

export const Card = styled.div`
    height: 250px;
    position: relative;
    img {
        width: 100%;
        height: 100%;
        border-radius: 5px;
    }
    div {
        position: absolute;
        background-color: rgba(0, 0, 0, 0.5);
        height: 250px;
        width: 100%;
        top: 0;
        border-radius: 5px;
        padding: 20px;
    }
    div > span {
        color: white;
        font-size: 28px;
        font-weight: 500;
    }
    cursor: pointer;
`;