import React from 'react';
import { useHistory } from 'react-router-dom';
import category_restaurant from '@/statics/images/category_restaurant.jpg';
import category_market from '@/statics/images/category_market.jpg';
import category_transportation from '@/statics/images/category_transportation.jpg';
import category_childcare from '@/statics/images/category_childcare.jpg';
import category_sports_leisures from '@/statics/images/category_sports_leisures.jpg';
import category_clothing from '@/statics/images/category_clothing.jpg';
import category_apartment from '@/statics/images/category_apartment.jpg';
import category_salary from '@/statics/images/category_salary.jpg';
import category_rent from '@/statics/images/category_rent.jpg';
import * as Style from './style';

const categoryObject = {
    Restaurants: { name: 'Restaurants', path: 'restaurants' },
    Markets: { name: 'Markets', path: 'markets' },
    Transportaiton: { name: 'Transportaiton', path: 'transportaiton' },
    Childcare: { name: 'Childcare', path: 'childcare' },
    SportAndLeisures: { name: 'Sports And Leisures', path: 'sportsleisures' },
    ClothingAndShoes: { name: 'Clothing And Shoes', path: 'clothingshoes' },
    BuyApartment: { name: 'Buy Apartment', path: 'buyapartment' },
    SalariesAndFinancing: { name: 'Salaries And Financing', path: 'salariesfinancing' },
    RentPerMonth: { name: 'Rent Per Month', path: 'rentpermonth' }
};

const categories = [
    { name: categoryObject.Restaurants.name, image: category_restaurant },
    { name: categoryObject.Markets.name, image: category_market },
    { name: categoryObject.Transportaiton.name, image: category_transportation },
    { name: categoryObject.Childcare.name, image: category_childcare },
    { name: categoryObject.SportAndLeisures.name, image: category_sports_leisures },
    { name: categoryObject.ClothingAndShoes.name, image: category_clothing },
    { name: categoryObject.BuyApartment.name, image: category_apartment },
    { name: categoryObject.SalariesAndFinancing.name, image: category_salary },
    { name: categoryObject.RentPerMonth.name, image: category_rent },
];

const Categories = () => {
    const history = useHistory();

    const handleCardClick = (category: string) => {
        switch (category) {
            case categoryObject.Restaurants.name:
                history.push(categoryObject.Restaurants.path);
                break;
            case categoryObject.Markets.name:
                history.push(categoryObject.Markets.path);
                break;
            case categoryObject.Transportaiton.name:
                history.push(categoryObject.Transportaiton.path);
                break;
            case categoryObject.Childcare.name:
                history.push(categoryObject.Childcare.path);
                break;
            case categoryObject.SportAndLeisures.name:
                history.push(categoryObject.SportAndLeisures.path);
                break;
            case categoryObject.ClothingAndShoes.name:
                history.push(categoryObject.ClothingAndShoes.path);
                break;
            case categoryObject.BuyApartment.name:
                history.push(categoryObject.BuyApartment.path);
                break;
            case categoryObject.SalariesAndFinancing.name:
                history.push(categoryObject.SalariesAndFinancing.path);
                break;
            default:
                history.push(categoryObject.RentPerMonth.path);
                break;
        }
    };

    return (
        <Style.Cards>
            {
                categories.map((category) => (
                    <Style.Card key={`${category.name}`} onClick={() => handleCardClick(category.name)}>
                        <img src={category.image} />
                        <div><span>{category.name}</span></div>
                    </Style.Card>
                ))
            }
        </Style.Cards>
    );
}

export default Categories;