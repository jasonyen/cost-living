import React from 'react';
import Categories from '@/components_portal/Categories/Categories';
import Footer from '@/components_portal/Footer/Footer';
import * as Style from './style';

const Portal = () => {
    return (
        <>
            <Style.Body>
                <Style.Title>Cost Living Around You</Style.Title>
                <Categories />
            </Style.Body>
            <Footer />
        </>
    );
}

export default Portal;