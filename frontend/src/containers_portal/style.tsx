import styled from "styled-components";

export const Body = styled.div`
    padding: 0 50px 50px 50px;
    background-color: #EDF6F9;
    height: auto;
`;

export const Title = styled.div`
    padding: 20px 20px 50px 20px;
    text-align: center;
    font-size: 38px;
    font-weight: 500;
`;

