import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    .name {
        font-size: 16px;
        display: flex;
        justify-content: start;
        align-items: center;
        margin-left: 10px;
    }
    .arrow {
        display: flex;
        justify-content: start;
        align-items: center;
        margin-left: 10px;
    }
    cursor: pointer;
`;
