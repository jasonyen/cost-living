import 'antd/dist/antd.less';
import React, { lazy, Suspense, useEffect } from "react";
import { render } from "react-dom";
import { HashRouter as Router, Route } from "react-router-dom";

const App = lazy(() => import("@/containers/App" /* webpackChunkName:"App" */));
const Login = lazy(() => import("@/containers/Login/Login" /* webpackChunkName:"Login" */));
const Portal = lazy(() => import("@/containers_portal/Portal" /* webpackChunkName:"Portal" */));
const Restaurants = lazy(() => import("@/containers_portal/Restaurants/Restaurants" /* webpackChunkName:"Restaurants" */));

const Main = () => (
    <Router>
        <Suspense fallback={<div>Module loading....</div>}>
            <Route path="/" exact component={Login} />
            <Route path="/app" component={App} />
            <Route path="/main" component={Portal} />
            <Route path="/restaurants" component={Restaurants} />
        </Suspense>
    </Router>
);

render(<Main />, document.getElementById("app"));