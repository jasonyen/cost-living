// interface IConfig {
//     FRONTEND: string
// }
const defaultConfig = {
    API_URL: 'http://localhost:3000/api' // for local test
    // API_URL: 'http://52.221.205.203/api' // for deploy to server
}
let FRONTEND : string = '';
export default FRONTEND ? JSON.parse(FRONTEND) : defaultConfig;
