import styled from "styled-components";
import { Table } from "antd"; 

export const HeaderContainer = styled.div`
    display: flex;
    justify-content: space-between;
    .block-left {
        width: 100%;
        text-align: left;
        padding-bottom: 10px;
    }
    .block-right {
        width: 100%;
        text-align: right;
        padding-bottom: 10px;
    }
`;

export const AntdTable = styled(Table)`
    .ant-table-thead > tr > th {
        background: gray;
    }
`;

export const PaginationContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 10px;
`;
