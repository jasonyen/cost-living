import React from 'react';
import { Table, Pagination, Button } from 'antd';
import type { ColumnsType } from 'antd/lib/table';
import HttpServices, { httpStatus } from '@/services/HttpServices';
import { queryString } from '@/utils/Maps';
import * as Style from './style';
import './antd.css';

interface DataType {
    country: string;
    city: string;
};

const CountrySetting = () => {
    const [tableData, setTableData] = React.useState<DataType[]>([]);
    const [totalData, setTotalData] = React.useState(0);
    const [currentPage, setCurrentPage] = React.useState(1);

    const columns: ColumnsType<DataType> = [
        {
            title: 'Country',
            dataIndex: 'country',
            key: 'country',
        },
        {
            title: 'City',
            dataIndex: 'city',
            key: 'city',
        }
    ];

    const fetchData = async (page: number) => {
        const httpServices = HttpServices();
        try {
            const request = {
                url: '/citysetting/getPaginationCity',
                method: 'post',
                data: {
                    pageSize: 10,
                    page,
                    country: queryString.All,
                    city: queryString.All
                }
            };
            const response = await httpServices.exec(request);
            if (response.status === httpStatus.Success) {
                setTableData(response.result.data);
                setTotalData(response.result.total);
            }
        } catch (error) {
            console.error('Login error: ', error);
        }
    };

    const handlePaginationChange = (page: number) => {
        setTableData([]);
        setCurrentPage(page);
        fetchData(page);
    };

    React.useEffect(() => {
        fetchData(1);
    }, [0]);

    return (
        <div>
            <Style.HeaderContainer>
                <div className='block-left'>cc</div>
                <div className='block-right'><Button type="primary">Add City</Button></div>
            </Style.HeaderContainer>
            <Table
                bordered
                loading={tableData.length == 0}
                columns={columns}
                dataSource={tableData}
                pagination={false}
            />
            <Style.PaginationContainer>
                <Pagination
                    defaultCurrent={1}
                    current={currentPage}
                    total={totalData}
                    pageSize={10}
                    pageSizeOptions={[10]}
                    hideOnSinglePage={true}
                    onChange={handlePaginationChange}
                />
            </Style.PaginationContainer>
        </div>
    );
}

export default CountrySetting;