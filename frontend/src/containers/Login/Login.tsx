import React from 'react';
import HttpServices from '@/services/HttpServices';
import { History, LocationState } from 'history';
import { Button } from 'antd';
import * as Style from './style';

interface IProps {
    history: History<LocationState>,
    setToken: Function
}

interface IAccessToken {
    status: number,
    result: string
}

const Login = ({ history }: IProps) => {
    const httpServices = HttpServices();
    const handleLogin = async () => {
        try {
            const login = {
                url: '/loginCallback',
                method: 'post',
                data: {
                    userId: 'E0001',
                    name: 'Jason',
                    email: 'jasonyen2008@gmail.com'
                }
            };
            await httpServices.exec(login) as IAccessToken;
            history.push('app/countrysetting');
        } catch (error) {
            console.error('Login error: ', error);
        }
    };

    const handlePortal = () => { history.push('/portal'); };

    return (
        <Style.LoginBackground>
            <Button type="primary" size="large" onClick={handleLogin}>Click to login</Button>
            <br />
            <Button type="primary" size="large" onClick={handlePortal}>Go to portal</Button>
        </Style.LoginBackground>
    )
};

export default Login;