import {
    DesktopOutlined,
    FileOutlined,
    PieChartOutlined,
    TeamOutlined,
    UserOutlined,
} from '@ant-design/icons';
import type { MenuProps } from 'antd';
import { Layout, Menu } from 'antd';
import React, { useState, useEffect } from 'react';
import { HashRouter, Route, Switch, useHistory } from "react-router-dom";
import CountrySetting from '@/containers/CountrySetting/CountrySetting';
import CostItemSetting from '@/containers/CostItemSetting/CostItemSetting';
import HttpServices from '@/services/HttpServices';
import UserAvatar from '@/components/UserAvatar/UserAvatar';
import * as Style from './style';
import './antd.css';

const { Content, Footer, Sider } = Layout;

type MenuItem = Required<MenuProps>['items'][number];

function getItem(
    label: React.ReactNode,
    key: React.Key,
    icon?: React.ReactNode,
    children?: MenuItem[],
): MenuItem {
    return {
        key,
        icon,
        children,
        label,
    } as MenuItem;
}

const items: MenuItem[] = [
    getItem('Country Setting', '1', <PieChartOutlined />),
    getItem('Cost Item Setting', '2', <DesktopOutlined />),
    // getItem('User', 'sub1', <UserOutlined />, [
    //     getItem('Tom', '3'),
    //     getItem('Bill', '4'),
    //     getItem('Alex', '5'),
    // ]),
    // getItem('Team', 'sub2', <TeamOutlined />, [getItem('Team 1', '6'), getItem('Team 2', '8')]),
    // getItem('Files', '9', <FileOutlined />),
];

const App = () => {
    const history = useHistory();
    const [collapsed, setCollapsed] = useState(false);
    const [accessToken, setAccessToken] = useState('');

    // const setToken = (accessToken: string) => {
    //     localStorage.setItem('accessToken', accessToken);
    //     setAccessToken(accessToken);
    // };

    // const getToken = () => {
    //     const tokenString = localStorage.getItem('accessToken');
    //     return tokenString;
    // };

    // if (accessToken === '[object Object]' || typeof accessToken === 'object') {
    //     return <Login setToken={setToken} />
    // }

    const handleMenuClick = (event: any) => {
        localStorage.setItem('selectedMenuItem', event.key);
        switch (event.key) {
            case '1':
                history.push('/app/countrysetting');
                break;
            case '2':
                history.push('/app/costitemsetting');
                break;
            default:
                history.push('/app');
                break;
        }
    };

    // const authorize = async () => {
    //     const httpServices = HttpServices();
    //     const config = {
    //         url: '/authorize',
    //         method: 'get'
    //     };
    //     const result = await httpServices.exec(config);
    //     if (result.status == 500) {
    //         history.push('/');
    //     }
    // };


    // useEffect(() => {
    //     authorize();
    // }, [0]);

    return (
        <Layout style={{ minHeight: '100vh' }}>
            <Sider collapsible collapsed={collapsed} onCollapse={value => setCollapsed(value)}>
                <Style.Logo />
                <Menu
                    theme="dark"
                    defaultSelectedKeys={localStorage.getItem('selectedMenuItem') ? [localStorage.getItem('selectedMenuItem') as string] : ['1']}
                    mode="inline"
                    items={items}
                    onClick={handleMenuClick}
                />
            </Sider>
            <Layout style={{ background: '#F6F7FB' }}>
                <Style.Header><UserAvatar history={history} /></Style.Header>
                <Content style={{ padding: '25px', maxHeight: 'calc(100vh - 60px)', overflowY: 'scroll' }}>
                    <HashRouter>
                        <Switch>
                            <Route exact path="/app/countrysetting">
                                <CountrySetting />
                            </Route>
                            <Route path="/app/costitemsetting">
                                <CostItemSetting />
                            </Route>
                        </Switch>
                    </HashRouter>
                </Content>
                {/* <Footer style={{ textAlign: 'center' }}>Amazing Living ©2022 Created by Jason Chang</Footer> */}
            </Layout>
        </Layout>
    );
};

export default App;