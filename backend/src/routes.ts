import express, { RequestHandler } from 'express';
import * as AuthController from './controllers/AuthController';
import * as PostController from './controllers/PostController';
import * as WebCrawlerController from './controllers/WebCrawlerController';
import * as SchedulerController from './controllers/SchedulerController';
import * as CitySettingController from './controllers/CitySettingController';
import { isAuthorized } from './middlewares/Passport';
const routers = express.Router();

interface IRouteItem {
    path: string;
    method: 'get' | 'post' | 'put' | 'delete';
    middlewares: RequestHandler[];
};

const PREFIX = {
    AUTHORIZE: '/authorize',
    POST: '/post',
    WEB_CRAWLER: '/crawler',
    SCHEDULER: '/scheduler',
    CITYSETTING: '/citysetting'
};

export const AppRoutes: IRouteItem[] = [
    {
        path: `/loginCallback`,
        method: 'post',
        middlewares: [
            AuthController.loginCallback,
        ],
    },
    {
        path: `/logout`,
        method: 'post',
        middlewares: [
            AuthController.logout
        ]
    },
    {
        path: `${PREFIX.POST}/getPosts`,
        method: 'get',
        middlewares: [
            // isAuthorized,
            PostController.getPosts,
        ],
    },
    {
        path: `${PREFIX.WEB_CRAWLER}/getExpatistanData`,
        method: 'post',
        middlewares: [
            WebCrawlerController.getExpatistanData,
        ],
    },
    {
        path: `${PREFIX.WEB_CRAWLER}/getAllCountries`,
        method: 'get',
        middlewares: [
            isAuthorized,
            WebCrawlerController.getAllCountries,
        ],
    },
    {
        path: `${PREFIX.SCHEDULER}/triggerFetchExpatistanData`,
        method: 'get',
        middlewares: [
            SchedulerController.triggerFetchExpatistanData,
        ],
    },
    {
        path: `${PREFIX.AUTHORIZE}`,
        method: 'get',
        middlewares: [
            isAuthorized,
            AuthController.checkAuthorize
        ],
    },
    {
        path: `${PREFIX.CITYSETTING}/getPaginationCity`,
        method: 'post',
        middlewares: [
            isAuthorized,
            CitySettingController.getPaginationCity
        ]
    },
    {
        path: `${PREFIX.CITYSETTING}/createCity`,
        method: 'post',
        middlewares: [
            isAuthorized,
            CitySettingController.createCity
        ]
    }
];
