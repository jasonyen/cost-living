import express from 'express';
import { loginCallback } from '../controllers/AuthController';
const routers = express.Router();

routers.post('/loginCallback', loginCallback);

export default routers;