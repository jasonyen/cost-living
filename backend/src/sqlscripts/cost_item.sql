INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Restaurants', 'Meal, Inexpensive Restaurant');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Restaurants', 'Meal for 2 People, Mid-range Restaurant, Three-course');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Restaurants', 'McMeal at McDonalds (or Equivalent Combo Meal)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Restaurants', 'Domestic Beer (0.5 liter draught)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Restaurants', 'Imported Beer (0.33 liter bottle)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Restaurants', 'Cappuccino (regular)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Restaurants', 'Coke/Pepsi (0.33 liter bottle)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Restaurants', 'Water (0.33 liter bottle)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Markets', 'Milk (regular), (1 liter)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Markets', 'Loaf of Fresh White Bread (500g)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Markets', 'Rice (white), (1kg)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Markets', 'Eggs (regular) (12)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Markets', 'Local Cheese (1kg)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Markets', 'Chicken Fillets (1kg)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Markets', 'Beef Round (1kg) (or Equivalent Back Leg Red Meat)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Markets', 'Apples (1kg)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Markets', 'Banana (1kg)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Markets', 'Oranges (1kg)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Markets', 'Tomato (1kg)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Markets', 'Potato (1kg)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Markets', 'Onion (1kg)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Markets', 'Lettuce (1 head)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Markets', 'Water (1.5 liter bottle)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Markets', 'Bottle of Wine (Mid-Range)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Markets', 'Domestic Beer (0.5 liter bottle)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Markets', 'Imported Beer (0.33 liter bottle)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Markets', 'Cigarettes 20 Pack (Marlboro)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Transportation', 'One-way Ticket (Local Transport)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Transportation', 'Monthly Pass (Regular Price)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Transportation', 'Taxi Start (Normal Tariff)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Transportation', 'Taxi 1km (Normal Tariff)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Transportation', 'Taxi 1hour Waiting (Normal Tariff)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Transportation', 'Gasoline (1 liter)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Transportation', 'Volkswagen Golf 1.4 90 KW Trendline (Or Equivalent New Car)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Transportation', 'Toyota Corolla Sedan 1.6l 97kW Comfort (Or Equivalent New Car)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Utilities (Monthly)', 'Basic (Electricity, Heating, Cooling, Water, Garbage) for 85m2 Apartment');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Utilities (Monthly)', '1 min. of Prepaid Mobile Tariff Local (No Discounts or Plans)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Utilities (Monthly)', 'Internet (60 Mbps or More, Unlimited Data, Cable/ADSL)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Sports And Leisure', 'Fitness Club, Monthly Fee for 1 Adult');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Sports And Leisure', 'Tennis Court Rent (1 Hour on Weekend)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Sports And Leisure', 'Cinema, International Release, 1 Seat');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Childcare', 'Preschool (or Kindergarten), Full Day, Private, Monthly for 1 Child');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Childcare', 'International Primary School, Yearly for 1 Child');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Clothing And Shoes', '1 Pair of Jeans (Levis 501 Or Similar)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Clothing And Shoes', '1 Summer Dress in a Chain Store (Zara, H&M, ...)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Clothing And Shoes', '1 Pair of Nike Running Shoes (Mid-Range)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Clothing And Shoes', '1 Pair of Men Leather Business Shoes');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Rent Per Month', 'Apartment (1 bedroom) in City Centre');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Rent Per Month', 'Apartment (1 bedroom) Outside of Centre');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Rent Per Month', 'Apartment (3 bedrooms) in City Centre');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Rent Per Month', 'Apartment (3 bedrooms) Outside of Centre');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Buy Apartment Price', 'Price per Square Meter to Buy Apartment in City Centre');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Buy Apartment Price', 'Price per Square Meter to Buy Apartment Outside of Centre');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Salaries And Financing', 'Average Monthly Net Salary (After Tax)');
INSERT INTO public.cost_item
(item_type, item_name)
VALUES('Salaries And Financing', 'Mortgage Interest Rate in Percentages (%), Yearly, for 20 Years Fixed-Rate');
