INSERT INTO public.city
(country, city)
VALUES('Taiwan', 'Kaohsiung');
INSERT INTO public.city
(country, city)
VALUES('Taiwan', 'Tainan');
INSERT INTO public.city
(country, city)
VALUES('Taiwan', 'Taipei');
INSERT INTO public.city
(country, city)
VALUES('Taiwan', 'Taoyuan');
INSERT INTO public.city
(country, city)
VALUES('Taiwan', 'HsinChu');
INSERT INTO public.city
(country, city)
VALUES('Taiwan', 'Miao-li-Taiwan');
INSERT INTO public.city
(country, city)
VALUES('Taiwan', 'Taichung');
INSERT INTO public.city
(country, city)
VALUES('Taiwan', 'Chang-hua-Taiwan');
INSERT INTO public.city
(country, city)
VALUES('Taiwan', 'Yunlin-Taiwan');
INSERT INTO public.city
(country, city)
VALUES('Taiwan', 'Chiayi-Taiwan');
INSERT INTO public.city
(country, city)
VALUES('Taiwan', 'Pingtung-city-Taiwan');
INSERT INTO public.city
(country, city)
VALUES('Taiwan', 'Taitung');
INSERT INTO public.city
(country, city)
VALUES('Taiwan', 'Hua-lien');
INSERT INTO public.city
(country, city)
VALUES('Taiwan', 'Yilan-Taiwan');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Aberdeen');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Bangor-United-Kingdom');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Bath');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Belfast');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Birmingham');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Bradford');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Brighton');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Bristol');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Cambridge');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Canterbury');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Cardiff');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Carlisle-United-Kingdom');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Chelmsford');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Chester');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Chichester-United-Kingdom');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Coventry');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Derby');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Dundee');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Edinburgh');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Exeter');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Glasgow');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Gloucester');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Hereford-United-Kingdom');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Inverness');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'London');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Lancaster');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Leeds');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Leicester');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Lichfield-United-Kingdom');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Lisburn-United-Kingdom');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Liverpool');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Manchester');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Newport');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Newry-United-Kingdom');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Norwich');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Nottingham');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Oxford');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Peterborough');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Plymouth');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Portsmouth');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Preston');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Salford-United-Kingdom');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Salisbury');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Sheffield');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Southampton');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Stirling-United-Kingdom');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Sunderland');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Swansea');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Truro');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Wakefield-United-Kingdom');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Wells-United-Kingdom');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Westminster-United-Kingdom');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Winchester-United-Kingdom');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'Wolverhampton');
INSERT INTO public.city
(country, city)
VALUES('United Kingdom', 'York');
INSERT INTO public.city
(country, city)
VALUES('Ireland', 'Armagh-United-Kingdom');
