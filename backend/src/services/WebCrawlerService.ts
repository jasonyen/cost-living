import { Request, Response, NextFunction } from 'express';
import dbConfig from '../utils/env';
const request = require('request');
const cheerio = require('cheerio');
const { Client } = require('pg');

interface IPrice {
    city: string,
    type: string,
    item: string,
    price: number,
    currency: string,
    createTime: string
};

interface ICostItem {
    item_type: string,
    item_name: string
};

const currency = 'NTD';
export const getExpatistanData = (city: string) => {
    return new Promise((resolve, reject) => {
        try {
            const url = `https://www.numbeo.com/cost-of-living/in/${city}?displayCurrency=TWD`;
            request(url, async (error: any, response: any, html: any) => {
                // Reject promise if there exists error
                if (error && response.statusCode !== 200) {
                    reject(error);
                } else {
                    const $ = cheerio.load(html);
                    // Get all cost items
                    const allCostItems = await getCostItems();

                    // items
                    const items: string[] = [];
                    $('.data_wide_table tr > td:nth-child(-n+1)').each((index: any, element: any) => {
                        // console.log('odd: ', index, $(element).text());
                        if (index !== 4) items.push($(element).text().trim()); // exclude Imported Beer (0.33 liter bottle)
                    });
                    // console.log('items', items);

                    // item price
                    const itemPrice: string[] = [];
                    $('.data_wide_table tr > td:nth-child(even)').each((index: any, element: any) => {
                        // console.log('even: ', index, $(element).text().trim().split('NT$')[0].trim());
                        if (index !== 4) itemPrice.push(($(element).text().trim().split('NT$')[0].trim()));
                    });
                    // console.log('itemPrice', itemPrice);

                    for (let i = 0; i < items.length; i++) {
                        const record: IPrice = {
                            city,
                            type: allCostItems.find((item: ICostItem) => item.item_name === items[i]).item_type,
                            item: items[i],
                            price: getPrice(itemPrice[i] === '?' ? '0' : itemPrice[i]),
                            currency,
                            createTime: new Date().toLocaleDateString('zh-Hans-CN')
                        };
                        // console.log('record', record)
                        const costLivingSqlValues = [record.city, record.type, record.item, record.price, record.currency, record.createTime];
                        const costLivingSqlScript = 'insert into cost_living(city, item_type, item_name, price, currency, create_time) values($1, $2, $3, $4, $5, $6);';

                        let client = new Client(dbConfig);
                        // insert into cost_living
                        try {
                            client.connect();
                            await client.query(costLivingSqlScript, costLivingSqlValues);
                            client.end();
                        } catch (error) {
                            client.end();
                            // reject(error);
                        }
                    }
                    resolve({ message: `${city} ${new Date().toLocaleDateString()} data insert completely`, status: 200 });
                }
            });
        } catch (error) {
            reject({ message: `${city} ${new Date().toLocaleDateString()} data insert failed`, status: 400 });
        }
    });
};

export const getAllCountries = () => {
    return new Promise(async (resolve, reject) => {
        try {
            const client = new Client(dbConfig);
            client.connect();
            const res = await client.query('select * from country;');
            client.end();
            resolve(res.rows);
        } catch (error) {
            reject(error);
        }
    });
};

export const getAllCities = () => {
    return new Promise(async (resolve, reject) => {
        try {
            const client = new Client(dbConfig);
            client.connect();
            const res = await client.query('select * from city;');
            client.end();
            resolve(res.rows);
        } catch (error) {
            reject(error);
        }
    });
};

const getCostItems = async () => {
    try {
        const client = new Client(dbConfig);
        client.connect();
        const costItems = await client.query('select * from cost_item');
        client.end();
        return costItems.rows;
    } catch (error) {
        console.error('****getCostItems error****', error);
    }
};

/*
    Convert string price to integer
*/
const getPrice = (price: string) => {
    let correctPrice = '';
    const priceSplit = price.split(',');
    for (let i = 0; i < priceSplit.length; i++) {
        correctPrice += priceSplit[i];
    }
    return parseInt(correctPrice);
};
