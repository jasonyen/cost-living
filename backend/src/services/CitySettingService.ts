import dbConfig from '../utils/env';
import { SqlStates } from '../utils/Map';
const { Client } = require('pg');

export const getPaginationCity = (pageSize: number, page: number, country: string, city: string) => {
    return new Promise(async (resolve, reject) => {
        try {
            const client = new Client(dbConfig);
            client.connect();
            const sqlValue = [pageSize, page == 1 ? 0 : (page - 1) * pageSize, country, city];
            let sqlScript = '';

            if (country === SqlStates.All && city === SqlStates.All) {
                sqlScript = 'select * from city where $3 = $3 and $4 = $4 order by id limit $1 offset $2';
            } else if (country === SqlStates.All && city !== SqlStates.All) {
                sqlScript = 'select * from city'
                    + ' where city = $4 and $3 = $3'
                    + ' order by id limit $1 offset $2';
            } else if (country !== SqlStates.All && city === SqlStates.All) {
                sqlScript = 'select * from city'
                    + ' where country = $3 and $4 = $4'
                    + ' order by id limit $1 offset $2';
            } else {
                sqlScript = 'select * from city'
                    + ' where country = $3 and city = $4'
                    + ' order by id limit $1 offset $2';
            }

            const res = await client.query(sqlScript, sqlValue);
            client.end();
            resolve(res.rows);
        } catch (error) {
            reject(error);
        }
    });
};

export const getCityCount = (country: string, city: string) => {
    return new Promise(async (resolve, reject) => {
        try {
            const client = new Client(dbConfig);
            client.connect();
            const sqlValue = [country, city];
            let sqlScript = '';

            if (country === SqlStates.All && city === SqlStates.All) {
                sqlScript = 'select count(*) from city where $1 = $1 and $2 = $2;';
            } else if (country === SqlStates.All && city !== SqlStates.All) {
                sqlScript = 'select count(*) from city'
                    + ' where city = $2 and $1 = $1;';
            } else if (country !== SqlStates.All && city === SqlStates.All) {
                sqlScript = 'select count(*) from city'
                    + ' where country = $1 and $2 = $2;';
            } else {
                sqlScript = 'select count(*) from city'
                    + ' where country = $1 and city = $2;';
            }

            const res = await client.query(sqlScript, sqlValue);
            client.end();
            resolve(res.rows[0].count);
        } catch (error) {
            reject(error);
        }
    });
};

export const createCity = (country: string, city: string) => {
    return new Promise(async (resolve, reject) => {
        try {
            const client = new Client(dbConfig);
            client.connect();
            const sqlValue = [country, city];
            const sqlScript1 = 'select count(*) from city where country = $1 and city = $2;';
            const cityResponse = await client.query(sqlScript1, sqlValue);
            if (cityResponse.rows[0].count > 0) {
                client.end();
                resolve({ message: 'The city is already exist.' });
            } else {
                const sqlScript2 = 'insert into city (country, city) values($1, $2);';
                await client.query(sqlScript2, sqlValue);
                client.end();
                resolve({ message: '' });
            }
        } catch (error) {
            reject(error);
        }
    });
};
