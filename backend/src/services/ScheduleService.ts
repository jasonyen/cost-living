import { Request, Response, NextFunction } from 'express';
const schedule = require('node-schedule');

(function () {
    schedule.scheduleJob('0 1 * * *', function () {
        console.log('This job was supposed to run at ' + new Date());
    });
})();