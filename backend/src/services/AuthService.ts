import { Secret, sign } from 'jsonwebtoken';
import { WEB_SAMPLE_JWT_SECRET } from '../config/index';

interface IPayload {
    userId: string,
    name: string,
    email: string
};

export const loginCallback = (payload: IPayload) => {
    const result = sign(
        {
            userId: payload.userId,
            name: payload.name,
            email: payload.email
        },
        WEB_SAMPLE_JWT_SECRET,
        { expiresIn: 3600 }   // 單位是秒
    );
    return result;
};