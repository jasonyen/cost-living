import { Secret } from 'jsonwebtoken';
const secret = process.env['WEB_SAMPLE_JWT_SECRET'];
export const WEB_SAMPLE_JWT_SECRET = secret as Secret;