import * as path from 'path';
import * as dotenv from 'dotenv';

// 引入全域env環境變數
let envFileName = 'DEV.env';
dotenv.config({ path: path.resolve(__dirname, `../env/${envFileName}`) });

import http from 'http';
import express, { Express } from 'express';
import morgan from 'morgan';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import { AppRoutes } from './routes';
import * as SchedulerController from './controllers/SchedulerController';
const schedule = require('node-schedule');
const router: Express = express();

const corsOptions = {
    origin: ['http://localhost:8080', 'http://52.221.205.203'],  // Access-Control-Allow-Origin
    methods: ['GET', 'POST'],   // Access-Control-Allow-Methods
    allowedHeaders: ['Content-Type', 'Authorization'],  // Access-Control-Allow-Headers
    preflightContinue: false,
    optionsSuccessStatus: 200,
    credentials: true, // Access-Control-Allow-Credentials,
    maxAge: 3600 // Access-Control-Max-Age
};

/** Using cors policy */
router.use(cors(corsOptions));

/** Using cookie policy */
router.use(cookieParser());

/** Logging */
router.use(morgan('dev'));
/** Parse the request */
router.use(express.urlencoded({ extended: false }));
/** Takes care of JSON data */
router.use(express.json());

/** Middleware */
// router.use((req, res, next) => {
//     console.log("A new request received at " + Date.now());
//     next();
// });

/** web entry */
router.use('/', express.static(__dirname + '/dist'));
router.get('/', (req, res) => {
    res.sendFile(__dirname + '/dist/index.html');
});

/** Routes */
// register all application routes
AppRoutes.forEach((route) => {
    router[route.method](
        `/api${route.path}`,
        ...route.middlewares,
    );
});

// /** Run immediately */
// (function () {
//     // router.runMiddleware('/api/post/getPosts');
//     axios.get('http://localhost:3000/api/scheduler/triggerFetchExpatistanData')
//     // axios.get('http://52.221.205.203/api/scheduler/triggerFetchExpatistanData')
//     .then((res) => {
//         // console.log('Run immediately 2', res.data.result);
//     })
//     .catch((error) => {
//         console.log('Run immediately', error);
//     });
// })();

/**
 * **** Scrap NUMBEO data at 00:00:01 AM everyday ****
 */
schedule.scheduleJob('01 00 00 * * 0-6', async () => {
    SchedulerController.numbeoDataScraper();
});

/** Server */
const httpServer = http.createServer(router);
const PORT: any = process.env.PORT ?? 3000;
httpServer.listen(PORT, () => console.log(`The server is running on port ${PORT}`));
