import { Request, Response, NextFunction } from 'express';
import { httpStatus } from '../utils/httpStatus';
import * as WebCrawlerService from '../services/WebCrawlerService';

export const getExpatistanData = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { body } = req;
        const { city } = body;
        const result = await WebCrawlerService.getExpatistanData(city);
        // console.log('controller getExpatistanData', result);
        return res.status(httpStatus.Success).json(result);
    } catch (error) {
        next(error);
    }
};

export const getAllCountries = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const result = await WebCrawlerService.getAllCountries();
        // console.log('controller getAllCountries', result);
        return res.status(httpStatus.Success).json(result);
    } catch (error) {
        next(error);
    }
};
