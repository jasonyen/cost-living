import { Request, Response, NextFunction } from 'express';
import { setInterval } from 'timers';
import dbConfig from '../utils/env';
import moment from 'moment';
import { httpStatus } from '../utils/httpStatus';

const schedule = require('node-schedule');
const { Client } = require('pg');
import * as WebCrawlerService from '../services/WebCrawlerService';
interface Country {
    short_name: String;
    display_name: String;
    name: String;
};

interface City {
    country: string,
    city: string
};

export const triggerFetchExpatistanData = async (req: Request, res: Response, next: NextFunction) => {
    try {
        console.log('**** Scrap NUMBEO data at 00:00:01 AM everyday ****');
        const job = schedule.scheduleJob('00 58 16 * * 0-6', async () => { // '01 00 00 * * 0-6' => 00:00:01 on everyday
            const cities: [City] = await WebCrawlerService.getAllCities() as [City];
            let index = 0;
            const interval = setInterval(async () => {
                // Scrape data and then insert to cost_living table
                const result: any = await WebCrawlerService.getExpatistanData(cities[index].city);

                // insert a log
                const logSqlValues = [cities[index].city, 'insert daily data', result.status === httpStatus.Success ? 'Success' : 'Failed', moment().format('YYYY/MM/DD hh:mm:ss')];
                const logSqlScript = 'insert into scheduler_log(city, type, status, create_time) values($1, $2, $3, $4);';
                let client = new Client(dbConfig);
                client.connect();
                await client.query(logSqlScript, logSqlValues);
                client.end();
                index += 1;
                if (index === cities.length) clearInterval(interval);
            }, 80000);
        });

        return res.status(httpStatus.Success);
    } catch (error) {
        console.log('**** triggerFetchExpatistanData failed ****');
        next(error);
    }
};

export const numbeoDataScraper = async () => {
    try {
        const cities: [City] = await WebCrawlerService.getAllCities() as [City];
        let index = 0;
        const interval = setInterval(async () => {
            // Scrape data and then insert to cost_living table
            const result: any = await WebCrawlerService.getExpatistanData(cities[index].city);

            // insert a log
            const logSqlValues = [cities[index].city, 'insert daily data', result.status === httpStatus.Success ? 'Success' : 'Failed', moment().format('YYYY/MM/DD hh:mm:ss')];
            const logSqlScript = 'insert into scheduler_log(city, type, status, create_time) values($1, $2, $3, $4);';
            let client = new Client(dbConfig);
            client.connect();
            await client.query(logSqlScript, logSqlValues);
            client.end();
            index += 1;
            if (index === cities.length) clearInterval(interval);
        }, 80000);
    } catch (error) {
        console.log('**** triggerFetchExpatistanData failed ****');
    }
};
