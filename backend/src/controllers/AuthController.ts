import { Request, Response, NextFunction } from 'express';
import { httpStatus } from '../utils/httpStatus';
import * as AuthService from '../services/AuthService';

export const loginCallback = (req: Request, res: Response, next: NextFunction) => {
    try {
        const { body } = req;
        const result = AuthService.loginCallback(body);
        return res
            .cookie("accessToken", result, { httpOnly: true, sameSite: 'strict' })
            .status(httpStatus.Success)
            .json({ message: 'Logged in successfully' });
    } catch (error) {
        next(error);
    }
};

export const checkAuthorize = (req: Request, res: Response, next: NextFunction) => {
    try {
        return res.status(httpStatus.Success).json({ message: 'Authorized' });
    } catch (error) {
        next(error);
    }
};

export const logout = (req: Request, res: Response) => {
    res.clearCookie('accessToken');
    return res.status(httpStatus.Success).json({ message: 'Logout successfully' });
};
