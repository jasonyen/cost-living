import { Request, Response, NextFunction } from 'express';
import { httpStatus } from '../utils/httpStatus';
import * as CitySettingService from '../services/CitySettingService';

interface IResult {
    message: string
}

export const getPaginationCity = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { body } = req;
        const { pageSize, page, country, city } = body;
        const result = await CitySettingService.getPaginationCity(pageSize, page, country, city);
        const count = await CitySettingService.getCityCount(country, city);
        return res.status(httpStatus.Success).json({
            total: count,
            data: result
        });
    } catch (error) {
        next(error);
    }
};

export const createCity = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { body } = req;
        const { country, city } = body;
        const result = await CitySettingService.createCity(country, city) as IResult;
        return res.status(httpStatus.Success).json({ message: result.message });
    } catch (error) {
        next(error);
    }
};
